# USBFD-tools

## Description
Management tools for floppy disk images on USB sticks, to be used with the
GoTek floppy disk emulator running its native formware.

These command-line tools provide a convenient means of reading, writing,
and erasing these FDD images on a USB stick, as well as banks of 100 FDD
images. They operate with the data format used by the native GoTek firmware
(not FlashFloppy or HxC).

These tools were written because the best available GUI tool for managing
floppy disk images on USB sticks for the GoTek, _USB Floppy Emulator 1.40_,
runs only on Windows, can only access floppy images 0 - 99 (while the GoTek
can use images 0 - 999), and crashes on attempts to read and write the USB
stick if certain FDD images (eg. non-FAT filesystems such as for Linux and
the BSDs) are written there.

Thanks to Lui Gough for researching the storage format used by the goTek
firmware (https://goughlui.com/2013/04/24/review-unbranded-1-44mb-usb-100-floppy-emulator/).

Cf. also the Windows-specific command lines tools provided by
https://github.com/xtcrefugee/gotek-usb-batch-files.

## Installation
Before using these scripts on your system, ascertain the block device name
that is assigned to your USB drive on your computer. (On many systems, `cat
/proc/partitions` will tell you this.) You can supply this device name as
the last argument to each script, but you can also make it the default by
editing each of the scripts and inserting this value where you see
_CHANGEME_.

The *write* and *erase* scripts write directly to the block device, so
destructive mistakes are possible if you get this wrong -- in particular
you can erase large chunks of your SSDs or HDDs; be careful.

The author disclaims all liability for any use of these tools, as detailed
in the attached LICENSE file, and you accept this as a condition of your
license to use the tools. If you don't accept that, you do not have
permission to use them.

## Usage

The USB stick may need to be formatted (with Gotek's native GUI or with the
emulator hardware itself) prior to first use.

All the scripts document their command line arguments if run with no args.

The scripts `usbfd-read-image.sh` and `usbfd-write-image.sh` respectively
read and write a single FDD image to a specific numbered slot on the USB
device. Images are numbered 0-999.

The scripts `usbfd-read-bank.sh` and `usbfd-write-bank.sh` respectively
read and write "banks" of 100 FDD images. Banks are number 0 to 9.

The scripts `usbfd-erase-image.sh` and `usbfd-erase-bank.sh` respectively
erase (zeroify) a specific image or bank. These are useful in case you want
to use _USB Floppy Emulator 1.40_ to manipulate your USB key, but it is
crashing because it doesn't like one of the images you have stored.

To copy one image in a slot on the USB stick to another, or to copy an
entire bank to another, first read it to a file and then write that file.

Copying banks can be convenient if you are using _USB Floppy Emulator 1.40_
to populate FAT images with files -- you can fill up all of bank 0 (images
0 - 99, i.e. the only ones the _USB Floppy Emulator 1.40_ tool can see),
save that to a file, and then write it to a higher-numbered bank. You can
then replace what you had in bank 0 while still having access to the
previous contents on the GoTek.

## Creating FDD images
These tools do *not* provide a means of creating floppy disk images or of
moving files from your host system into them. That can be done pretty
easily on Linux with _mkfs.msdos_ and the _mtools_ package. On Windows, the
same tools exist under Cygwin and WSL. The _USB Floppy Emulator 1.40_,
buggy as it is, can also do this, as can tools such as _WinImage_.

## Internals
The scripts are just wrappers around `dd` with a little bit of arithmetic
and a few simple sanity checks.

Internally these scripts do things such as:

   `dd if=/dev/sdb1 of=bank2-backup.bin bs=512 count=307200 skip=614400`

to capture the third bank (bank 2) on a USB stick to a file.

## Contributing
Pull requests are welcome.

## Author and copyright
Written by and Copyright (c) 2022 by Richard Reiner
richard AT richardreiner DOT com

## License
These scripts are made available under the GNU General Public License v2.0.
See the included `LICENSE` file for details.
