#!/bin/bash

# Read floppy disk images from a USB stick in the format used by the
# GoTek floppy disk emulator (native frmware, not FlashFloppy or HxC).

# Written by and Copyright (c) 2022 by Richard Reiner, richard AT richardreiner DOT com
# Made available under the GNU General Public License v2.0.
# See the attached `LICENSE` file for details.

MYNAME=`basename "$0"`

# IMPORTANT: set USBDEV to a sane default for your system!
# DANGER: Getting this wrong can overwrite parts of your HDD!
# This is often correct for a device with only one fixed disk:
#USBDEV=/dev/sdb1
USBDEV=CHANGEME

if [ "$USBDEV" = "CHANGEME" ]; then
    echo "$MYNAME: this script requires installation, please edit me"
    exit 5
fi

BLOCKBYTES=512
IMGOFFSET=3072   # Images are spaced this far apart (in blocks of size $BLOCKBYTES)
FDDBLOCKS=2880   # Images are typically of this size, up to a max of $IMGOFFSET. The firmware expects images to be spaced this far apart (in blocks of size $BLOCKBYTES)


usage () {
    echo "$MYNAME: usage is: $MYNAME imgfile slotnum [device]";
}

if [ "z$1" = "z" -o "z$2" = z ]; then
    usage
    exit 1
fi

IMGFILE="$1"

if [ "z$3" != "z" ]; then
    USBDEV="$3"
fi


if ! [[ "$2" =~ ^[0-9]+$ ]]; then
    echo "$MYNAME: slotnum must be a number between 0 and 999 inclusive"
    exit 3
fi

if [ "$2" -lt 0 -o "$2" -gt 999 ]; then
   echo "$MYNAME: slotnum must be between 0 and 999 inclusive"
   exit 4
else
    SLOTNUM="$2"
fi

OFFSET=$(( $IMGOFFSET * $SLOTNUM ))

dd if="$USBDEV" of="$IMGFILE" bs="$BLOCKBYTES" skip="$OFFSET" count="$FDDBLOCKS"
