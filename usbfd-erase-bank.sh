#!/bin/bash

# Erase (zeroify) a bank of 100 floppy disk images on a USB stick in the format used by the
# GoTek floppy disk emulator (native frmware, not FlashFloppy or HxC).

# Written by and Copyright (c) 2022 by Richard Reiner, richard AT richardreiner DOT com
# Made available under the GNU General Public License v2.0.
# See the attached `LICENSE` file for details.

MYNAME=`basename "$0"`

# IMPORTANT: set USBDEV to a sane default for your system!
# DANGER: Getting this wrong can overwrite parts of your HDD!
# This is often correct for a device with only one fixed disk:
#USBDEV=/dev/sdb1
USBDEV=CHANGEME

if [ "$USBDEV" = "CHANGEME" ]; then
    echo "$MYNAME: this script requires installation, please edit me"
    exit 5
fi

BLOCKBYTES=512
IMGOFFSET=3072   # Images are spaced this far apart (in blocks of size $BLOCKBYTES)
FDDBLOCKS=2880   # Images are typically of this size, up to a max of $IMGOFFSET. The firmware expects images to be spaced this far apart (in blocks of size $BLOCKBYTES)
BANKIMGCOUNT=100
BANKBLOCKS=$(( $IMGOFFSET * $BANKIMGCOUNT ))


usage () {
    echo "$MYNAME: usage is: $MYNAME banknum [device]";
}

if [ "z$1" = "z" ]; then
    usage
    exit 1
fi

if [ "z$2" != "z" ]; then
    USBDEV="$2"
fi

if ! [[ "$1" =~ ^[0-9]+$ ]]; then
    echo "$MYNAME: banknum must be a number between 0 and 999 inclusive"
    exit 3
fi

if [ "$1" -lt 0 -o "$1" -gt 9 ]; then
   echo "$MYNAME: banknum must be between 0 and 9 inclusive"
   exit 4
else
    BANKNUM="$1"
fi

OFFSET=$(( $IMGOFFSET * $BANKNUM * 100 ))

dd if=/dev/zero of="$USBDEV" bs="$BLOCKBYTES" seek="$OFFSET" count="$BANKBLOCKS" conv=notrunc
